# Tour Guide App

This android application is a simple tour guide app. The application is part of the "**Udacity Android Basic Nanodegre** - _(v3)Tour Guide app_" project.

[...]

## Pre-requisites

* Android SDK Version: 27
* Minimum SDK Version: 15
* Target SDK Version: 27